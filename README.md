Welcome to my project!

This project is about numerical analysis methods for approximation purposes.
I completed this project for the Numerical Analysis course in my university which I received 9/10 Points.

The following picture illustrates an example of the result of a root approximation of a given function, using the Secant Method.
![](images/sec_approximation.png)